# Installation
1. `pip install instalooter`

# requirements
1. os
2. sys
3. subprocess
4. datetime
5. argparse
5. instalooter

# How to run
1. run `main.py` (`python main.py`)

# custom user arguments
1. -o `output path` by defult is `/output+date`
1. -m `max number of target page posts` by defult is `10`

# Sample target_accounts
```txt
user1, user2, user3,...

```

# Sample admin_accounts
```txt
username1, password1
username2, password2
,...

```

# Notes
```txt
you can scrape instagram private pages if you have followed them.

```