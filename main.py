import os
import sys
import subprocess
import instalooter
from instalooter.looters import ProfileLooter
from datetime import datetime
import argparse
import urllib.request
import time

def instagram_scrape(admin_accounts, target_accounts, basepath, max_num_posts):
    for admin_acc in admin_accounts:
        for target_acc in target_accounts:
            directory = basepath + '/' + target_acc
            print(os.path.isdir(directory))
            if not os.path.exists(directory):
                os.makedirs(directory)
            command = ['instalooter' ,'-u' ,admin_acc[0] ,'-p' ,admin_acc[1] ,'user' ,target_acc , directory, '-d', '-T', '{date}.{datetime}', '-n', str(max_num_posts)]
            p = subprocess.Popen(command, stdout=subprocess.PIPE)
            output, err = p.communicate()
            looter = ProfileLooter(target_acc)
            for i, media in enumerate(looter.medias()):
                if i == int(max_num_posts):
                    break
                elif media['is_video']:
                    urllib.request.urlretrieve(media['display_url'], directory + '/' + str(i) + '.jpg')
                    time.sleep(1)
                    # print(media['display_url'])

def parse_arguments(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', type=str, help='Output base path.', default='./outputs_'+str(datetime.now().strftime('%Y-%m-%d-%H')))
    parser.add_argument('-m', type=str, help='max number of target page posts', default='10')
    return parser.parse_args(argv)

if __name__ == '__main__':
    
    args = parse_arguments(sys.argv[1:])

    admin_accounts = []  # (username, password) /n (username, password) /n ...
    target_accounts = [] # user1,user2,user3,...

    with open('admin_accounts.txt', 'r') as file:
        for line in file:
            line = line.replace('\n', ' ').replace(' ', '').split(',')
            admin_accounts.append((line[0], line[1]))

    with open('target_accounts.txt', 'r') as file:
        file = file.read().replace('\n', ' ').replace(' ', '').split(',')
        for i in file:
            target_accounts.append(i)

    print(admin_accounts)
    print(target_accounts)

    instagram_scrape(admin_accounts, target_accounts, args.o, args.m)

